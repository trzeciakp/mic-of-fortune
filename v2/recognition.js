var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent

const numbers = {'dwieście': 200, 'trzysta': 300, 'czterysta': 400, 'pięćset': 500, 'sześćset': 600, 'siedemset': 700, 'osiemset': 800, 'dziewięćset': 900, 'tysiąc': 1000, 'tysiąc sto': 1100, 'tysiąc dwieście': 1200, 'tysiąc trzysta': 1300, 'tysiąc czterysta': 1400, 'tysiąc pięćset': 1500, 'tysiąc sześćset': 1600, 'tysiąc siedemset': 1700, 'tysiąc osiemset': 1800, 'tysiąc dziewięćset': 1900, 'dwa tysiące': 2000, 'dwa tysiące sto': 2100, 'dwa tysiące dwieście': 2200, 'dwa tysiące trzysta': 2300, 'dwa tysiące czterysta': 2400, 'dwa tysiące pięćset': 2500, 'dwa tysiące sześćset': 2600, 'dwa tysiące siedemset': 2700, 'dwa tysiące osiemset': 2800, 'dwa tysiące dziewięćset': 2900, 'trzy tysiące': 3000, 'trzy tysiące sto': 3100, 'trzy tysiące dwieście': 3200, 'trzy tysiące trzysta': 3300, 'trzy tysiące czterysta': 3400, 'trzy tysiące pięćset': 3500, 'trzy tysiące sześćset': 3600, 'trzy tysiące siedemset': 3700, 'trzy tysiące osiemset': 3800, 'trzy tysiące dziewięćset': 3900, 'cztery tysiące': 4000, 'cztery tysiące sto': 4100, 'cztery tysiące dwieście': 4200, 'cztery tysiące trzysta': 4300, 'cztery tysiące czterysta': 4400, 'cztery tysiące pięćset': 4500, 'cztery tysiące sześćset': 4600, 'cztery tysiące siedemset': 4700, 'cztery tysiące osiemset': 4800, 'cztery tysiące dziewięćset': 4900, 'pięć tysięcy': 5000, 'pięć tysięcy sto': 5100, 'pięć tysięcy dwieście': 5200, 'pięć tysięcy trzysta': 5300, 'pięć tysięcy czterysta': 5400, 'pięć tysięcy pięćset': 5500, 'pięć tysięcy sześćset': 5600, 'pięć tysięcy siedemset': 5700, 'pięć tysięcy osiemset': 5800, 'pięć tysięcy dziewięćset': 5900, 'sześć tysięcy': 6000, 'sześć tysięcy sto': 6100, 'sześć tysięcy dwieście': 6200, 'sześć tysięcy trzysta': 6300, 'sześć tysięcy czterysta': 6400, 'sześć tysięcy pięćset': 6500, 'sześć tysięcy sześćset': 6600, 'sześć tysięcy siedemset': 6700, 'sześć tysięcy osiemset': 6800, 'sześć tysięcy dziewięćset': 6900, 'siedem tysięcy': 7000, 'siedem tysięcy sto': 7100, 'siedem tysięcy dwieście': 7200, 'siedem tysięcy trzysta': 7300, 'siedem tysięcy czterysta': 7400, 'siedem tysięcy pięćset': 7500, 'siedem tysięcy sześćset': 7600, 'siedem tysięcy siedemset': 7700, 'siedem tysięcy osiemset': 7800, 'siedem tysięcy dziewięćset': 7900, 'osiem tysięcy': 8000, 'osiem tysięcy sto': 8100, 'osiem tysięcy dwieście': 8200, 'osiem tysięcy trzysta': 8300, 'osiem tysięcy czterysta': 8400, 'osiem tysięcy pięćset': 8500, 'osiem tysięcy sześćset': 8600, 'osiem tysięcy siedemset': 8700, 'osiem tysięcy osiemset': 8800, 'osiem tysięcy dziewięćset': 8900, 'dziewięć tysięcy': 9000, 'dziewięć tysięcy sto': 9100, 'dziewięć tysięcy dwieście': 9200, 'dziewięć tysięcy trzysta': 9300, 'dziewięć tysięcy czterysta': 9400, 'dziewięć tysięcy pięćset': 9500, 'dziewięć tysięcy sześćset': 9600, 'dziewięć tysięcy siedemset': 9700, 'dziewięć tysięcy osiemset': 9800, 'dziewięć tysięcy dziewięćset': 9900, 'dziesięć tysięcy': 10000, 'dziesięć tysięcy sto': 10100, 'dziesięć tysięcy dwieście': 10200, 'dziesięć tysięcy trzysta': 10300, 'dziesięć tysięcy czterysta': 10400, 'dziesięć tysięcy pięćset': 10500, 'dziesięć tysięcy sześćset': 10600, 'dziesięć tysięcy siedemset': 10700, 'dziesięć tysięcy osiemset': 10800, 'dziesięć tysięcy dziewięćset': 10900, 'jedenaście tysięcy': 11000, 'jedenaście tysięcy sto': 11100, 'jedenaście tysięcy dwieście': 11200, 'jedenaście tysięcy trzysta': 11300, 'jedenaście tysięcy czterysta': 11400, 'jedenaście tysięcy pięćset': 11500, 'jedenaście tysięcy sześćset': 11600, 'jedenaście tysięcy siedemset': 11700, 'jedenaście tysięcy osiemset': 11800, 'jedenaście tysięcy dziewięćset': 11900, 'dwanaście tysięcy': 12000, 'dwanaście tysięcy sto': 12100, 'dwanaście tysięcy dwieście': 12200, 'dwanaście tysięcy trzysta': 12300, 'dwanaście tysięcy czterysta': 12400, 'dwanaście tysięcy pięćset': 12500, 'dwanaście tysięcy sześćset': 12600, 'dwanaście tysięcy siedemset': 12700, 'dwanaście tysięcy osiemset': 12800, 'dwanaście tysięcy dziewięćset': 12900, 'trzynaście tysięcy': 13000, 'trzynaście tysięcy sto': 13100, 'trzynaście tysięcy dwieście': 13200, 'trzynaście tysięcy trzysta': 13300, 'trzynaście tysięcy czterysta': 13400, 'trzynaście tysięcy pięćset': 13500, 'trzynaście tysięcy sześćset': 13600, 'trzynaście tysięcy siedemset': 13700, 'trzynaście tysięcy osiemset': 13800, 'trzynaście tysięcy dziewięćset': 13900, 'czternaście tysięcy': 14000, 'czternaście tysięcy sto': 14100, 'czternaście tysięcy dwieście': 14200, 'czternaście tysięcy trzysta': 14300, 'czternaście tysięcy czterysta': 14400, 'czternaście tysięcy pięćset': 14500, 'czternaście tysięcy sześćset': 14600, 'czternaście tysięcy siedemset': 14700, 'czternaście tysięcy osiemset': 14800, 'czternaście tysięcy dziewięćset': 14900, 'piętnaście tysięcy': 15000};

const numbersParts = ['vabank', "dwieście", "trzysta", "czterysta", "pięćset", "sześćset", "siedemset", "osiemset", "dziewięćset", "jeden", "tysiąc", "sto", "dwa", "tysiące", "trzy", "cztery", "pięć", "tysięcy", "sześć", "siedem", "osiem", "dziewięć", "dziesięć", "jedenaście", "dwanaście", "trzynaście", "czternaście", "piętnaście"]

const teams = ['żółci', 'niebiescy', 'mistrzowie', 'zieloni'];

var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + numbersParts.concat(teams).join(' | ') + ' ;';

var recognition = new SpeechRecognition();
var speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
//recognition.continuous = false;
recognition.lang = 'pl-PL';
recognition.interimResults = false;
recognition.maxAlternatives = 1;


var diagnostic = document.querySelector('.output');

const errors = {
  TEAM_NOT_RECOGNIZED: 'Nie rozpoznalem druzyny. Uslyszalem: ',
  BID_NOT_CORRECT: 'Błedna wartosc licytacji. Uslyszalem: ',
  COMMON_ERROR: 'Błedna licytacja. Uslyszalem: ',
};

const initialState = {
  teamsBalance: {
    'żółci': 5000, 'niebiescy': 5000, 'zieloni': 5000,
  },
  currentBids: {
    'żółci': 0, 'niebiescy': 0, 'zieloni': 0,
  },
  freePool: 0,
  isAuctionStarted: false,
};

const history = [];
let revertedHistory = [];

let state = JSON.parse(JSON.stringify(initialState));

function restorePreviousState() {
  state = JSON.parse(window.localStorage.getItem("state")) || JSON.parse(JSON.stringify(initialState));
}
restorePreviousState();

function bid(text) {
  const parts = text.split(" ");
  if (parts.length < 2) {
    return errors.COMMON_ERROR + text;
  }

  const team = parts[0].toLowerCase();
  if (!Object.keys(state.currentBids).includes(team)) {
    return errors.TEAM_NOT_RECOGNIZED + text;
  }

  const bidValueCandidate = parts.slice(1).join(' ');
  const bidValue = bidValueCandidate === 'Vabank' ? state.teamsBalance[team] : numbers[bidValueCandidate] || Object.values(numbers).find(n => n == bidValueCandidate);

  const maxBid = Math.max(...Object.values(state.currentBids));
  if (!bidValue || bidValue > state.teamsBalance[team] || bidValue < state.currentBids[team] || bidValue <= maxBid || (maxBid === state.currentBids[team] && maxBid > 200)) {
    return errors.BID_NOT_CORRECT + text;
  }
  saveState();

  state.currentBids[team] = bidValue;
  return { team, bidValue, correct: true };
}

function win() {
  const maxBid = Math.max(...Object.values(state.currentBids));
  if (maxBid === 200) {
    return '';
  }
  saveState();
  const pool = calculatePool();
  let winner = '';
  Object.keys(state.currentBids).forEach(team => {
    state.teamsBalance[team] -= state.currentBids[team];
    if (state.currentBids[team] === maxBid) {
      winner = team;
      state.teamsBalance[team] += pool
    }
    state.currentBids[team] = 0;
  });
  state.freePool = 0;
  state.isAuctionStarted = false;
  return winner;
}

function lose() {
  state.freePool = calculatePool();
  if (state.freePool === 0) {
    return;
  }
  saveState();
  Object.keys(state.currentBids).forEach(team => {
    state.teamsBalance[team] -= state.currentBids[team];
    state.currentBids[team] = 0;
  });
  state.isAuctionStarted = false;
}

hotkeys('p', function(event, handler){
  lose();
  refreshResults(`Druzyna przegrala, pieniadze zostaja w puli`);
});

hotkeys('w', function(event, handler){
  let winner = win();
  refreshResults(`<strong>${winner}</strong> wygrali, brawo ${winner}!`);
});

hotkeys('l', function(event, handler){
  startBid();
});

hotkeys('r', function(event, handler){
  saveState();
  state = JSON.parse(JSON.stringify(initialState));
  refreshResults(`>Zaczynamy od nowa<`);
});

hotkeys('u', function(event, handler){
  if (history.length > 0) {
    revertedHistory.push(state);
    state = history.pop();
    refreshResults(`Cofnieto ruch!`);
  } else {
    refreshResults(`Historia pusta!`);
  }
});

hotkeys('y', function(event, handler){
  if (revertedHistory.length > 0) {
    history.push(state);
    state = revertedHistory.pop();
    refreshResults(`Przywrocono ruch!`);
  } else {
    refreshResults(`Historia pusta!`);
  }
});

hotkeys('s', function(event, handler){
  start();
});

hotkeys('f', function(event, handler){
  startFinal();
});

hotkeys('1,2,3,4,5,6,7', function(event, handler){
  event.preventDefault();
  const key = parseInt(handler.key);
  const index = (key - 1) % 3;
  const team = Object.keys(state.currentBids)[index];
  switch (key) {
    case 1:
    case 2:
    case 3:
      team && edit(`bid-${team}`, team, CellTypes.BID); break;
    case 4:
    case 5:
    case 6:
      team && edit(`balance-${team}`, team, CellTypes.BALANCE); break;
    case 7:
      edit('pool', '', CellTypes.POOL); break;
    default: console.log(key);
  }
});

function saveState() {
  history.push(JSON.parse(JSON.stringify(state)));
  revertedHistory = [];
  window.localStorage.setItem("state", JSON.stringify(state));
  window.localStorage.setItem("history", JSON.stringify(history.slice(0,10)));
}

function start() {
  if (!state.isAuctionStarted) {
    saveState();
    Object.keys(state.currentBids).forEach(key => state.currentBids[key] = state.teamsBalance[key] >= 200 ? 200 : state.teamsBalance[key]);
    state.isAuctionStarted = true;
  }
  refreshResults(`Biore po 200 zlotych z konta kazdej druzyny i slucham panstwa`);
}

function startFinal() {
  const sum = Object.values(state.currentBids).reduce((a, b) => a + b);
  if (sum > 0) {
    refreshResults('Runda niezakonczona. Wcisnij <strong>p</strong> lub <strong>w</strong> aby zakonczyc runde');
    return;
  }
  if (Object.keys(state.currentBids).length == 2) {
    refreshResults('final juz wystartowany');
    return;
  }
  const maxBalance = Math.max(...Object.values(state.teamsBalance));
  const winners = Object.keys(state.teamsBalance).filter(team => state.teamsBalance[team] === maxBalance);
  if (winners.length > 1) {
    refreshResults(`Druzyny ${winners.join(' oraz ')} maja tyle samo punktow. Rozegraj dogrywke`);
    return;
  }
  changeTeamsToFinal(winners[0]);
}

function changeTeamsToFinal(team) {
  saveState();

  state.currentBids = {
    [team]: 0,
    'mistrzowie': 0,
  };

  state.teamsBalance = {
    [team]: state.teamsBalance[team],
    'mistrzowie': 10000,
  };

  refreshResults('Witamy druzyne mistrzow!');
}

function refreshResults(message) {
  diagnostic.innerHTML = `<p>${message}</p>${printStatus()}`;
}

function startBid() {
  start();
  recognition.start();
  console.log('Ready to receive a color command.');
}

const correctAudio = new Audio('audio/correct.mp3');

function markBidCorrect(team) {
  document.getElementById(`bid-${team}`).style.filter = 'brightness(150%)';
  correctAudio.play();
}

recognition.onresult = function(event) {
  // The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
  // The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
  // It has a getter so it can be accessed like an array
  // The [last] returns the SpeechRecognitionResult at the last position.
  // Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
  // These also have getters so they can be accessed like arrays.
  // The [0] returns the SpeechRecognitionAlternative at position 0.
  // We then return the transcript property of the SpeechRecognitionAlternative object

  var last = event.results.length - 1;
  var text = event.results[last][0].transcript;

  const bidResult = bid(text);
  const team = bidResult.team;
  const message = bidResult.correct ? `${team} ${bidResult.bidValue}!` : bidResult;
  refreshResults(`${message}`);
  if (bidResult.correct) {
    markBidCorrect(team);
  }
  console.log('Confidence: ' + event.results[0][0].confidence);
  console.log(state.currentBids);
};

const colors = {
  'żółci': '#765e1f',
  'niebiescy': '#00145e',
  'zieloni': '#334f30',
  'mistrzowie': '#000',
}

const CellTypes = {
  BID: 'BID',
  BALANCE: 'BALANCE',
  POOL: 'POOL',
};

function printStatus() {
  const bidRow = `<tr>${Object.keys(state.currentBids).map(team => `<td onclick="edit('bid-${team}', '${team}', '${CellTypes.BID}')" id="bid-${team}" style="background-color: ${colors[team]}" class="numbers">${state.currentBids[team]}</td>`).join('')}<td class="hidden" /></tr>`;
  const balanceRow = `<tr>${Object.keys(state.currentBids).map(team => `<td onclick="edit('balance-${team}', '${team}', '${CellTypes.BALANCE}')" id="balance-${team}" style="background-color: ${colors[team]}" class="numbers">${(state.teamsBalance[team] - state.currentBids[team])}</td>`).join('')}<td onclick="edit('pool', '', '${CellTypes.POOL}')" id="pool" style="background-color: #232321" class="numbers">${calculatePool()}</td></tr>`;
  return `<table class="results">
  <tr><td class="description" colspan="${Object.keys(state.currentBids).length}">LICYTACJA</td><td class="hidden" /></tr>
  ${bidRow}
  <tr>${Object.keys(state.currentBids).map(t => '<td class="description">STAN KONTA</td>')}<td class="description">PULA</td></tr>
  ${balanceRow}
</table>`
}

function calculatePool() {
  return Object.values(state.currentBids).reduce((a, b) => a+b) + state.freePool;
}

function edit(id, team, type) {
  refreshResults('Wlaczono tryb edycji!');
  const el = document.getElementById(id);
  el.innerHTML = `<form method="post" onSubmit=submitEdit(event)><input type="text" id="edit" value="${el.innerText}" /></form>`;
  const input = el.querySelector('form input');
  input.onblur = () => applyChanges(parseInt(input.value), team, type);
  input.focus();
}

function applyChanges(newValue, team, type) {
  switch (type) {
    case CellTypes.BID: applyBidChanges(newValue, team); break;
    case CellTypes.BALANCE: applyBalanceChanges(newValue, team); break;
    case CellTypes.POOL: applyPoolChanges(newValue, team); break;
  }
}

function applyBidChanges(newValue, team) {
  if (newValue <= state.teamsBalance[team]) {
    saveState();
    state.currentBids[team] = newValue;
    refreshResults('Edycja zakonczona sukcesem');
    markBidCorrect(team);
  } else {
    refreshResults('Nie udalo sie edytowac. Nowa wartosc zbyt duza!');
  }
}

function applyPoolChanges(newValue) {
  const newFreePool = newValue - calculatePool() + state.freePool;
  if (newFreePool >= 0) {
    saveState();
    state.freePool = newFreePool;
    refreshResults('Edycja zakonczona sukcesem');
  } else {
    refreshResults('Nie udalo sie edytowac. Nie mozna zmniejszyc puli ponizej zera');
  }
}

function applyBalanceChanges(newValue, team) {
  saveState();
  state.teamsBalance[team] = newValue + state.currentBids[team];
  refreshResults('Edycja zakonczona sukcesem');
}

function submitEdit(event) {
  event.preventDefault();
  event.target[0].blur();
}

recognition.onspeechend = function() {
  recognition.stop();
};

recognition.onnomatch = function(event) {
  refreshResults("Nie znaleziono");
};

recognition.onerror = function(event) {
  refreshResults('Wystapil blad: ' + event.error);
};
